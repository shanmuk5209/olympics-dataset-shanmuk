

function convertChartData(jsonData) {
  const property = Object.keys(jsonData);
  return property.reduce((returnData, currentValue) => {
    const finalData = {};
    finalData.name = currentValue;
    finalData.y = jsonData[currentValue];
    returnData.push(finalData);
    return returnData;
  }, []);
}
function chartHostsCity(hostsData) {
  const result = convertChartData(hostsData);
  Highcharts.chart('container', {
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      type: 'pie',
    },
    title: {
      text: 'No of Olympics Hosts per City',
    },
    tooltip: {
      pointFormat: '{series.name}: <b>{point.y}</b>',
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        dataLabels: {
          enabled: true,
          format: '<b>{point.name}</b>: {point.y} ',
        },
      },
    },
    series: [{
      name: 'Olympic Hosts',
      colorByPoint: true,
      data: result,
    }],
  });
}
function getMedals(medalsData, medal) {
  return Object.keys(medalsData).reduce((allMedals, eachMedal) => {
    allMedals.push(medalsData[eachMedal][medal]);
    return allMedals
  }, [])
}
function topMedalWinners(medalsData) {
 
  Highcharts.chart('container2', {
    chart: {
      type: 'column',
    },
    title: {
      text: 'Top 10 Medal Winner Countires',
    },
    xAxis: { categories: Object.keys(medalsData) },
    yAxis: {
      min: 0,
      title: {
        text: 'Total No of Medals',
      },
      stackLabels: {
        enabled: true,
        style: {
          fontWeight: 'bold',
          color: ( // theme
            Highcharts.defaultOptions.title.style
                        && Highcharts.defaultOptions.title.style.color
          ) || 'gray',
        },
      },
    },
    legend: {
      align: 'right',
      x: -30,
      verticalAlign: 'top',
      y: 25,
      floating: true,
      backgroundColor:
                Highcharts.defaultOptions.legend.backgroundColor || 'white',
      borderColor: '#CCC',
      borderWidth: 1,
      shadow: false,
    },
    tooltip: {
      headerFormat: '<b>{point.x}</b><br/>',
      pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}',
    },
    plotOptions: {
      column: {
        stacking: 'normal',
        dataLabels: {
          enabled: true,
        },
      },
    },
    series: [{
      name: 'Gold',
      data: getMedals(medalsData,'Gold'),
    }, {
      name: 'Silver',
      data: getMedals(medalsData,'Silver'),
    }, {
      name: 'Bronze',
      data: getMedals(medalsData,'Bronze'),
    }],
  });
}
function mfParticipation(mfData) {
  const male = [];
  const female = [];
  function getGender(gender) {
    Object.keys(mfData).forEach((key) => {
      if (gender === 'Male') {
        male.push(mfData[key][gender]);
      }
      if (gender === 'Female') {
        female.push(mfData[key][gender]);
      }
    });
  }
  getGender('Male');
  getGender('Female');
  Highcharts.chart('container3', {
    chart: {
      type: 'column',
    },
    title: {
      text: 'Male And Female Participation in Olympics',
    },
    xAxis: {
      categories: Object.keys(mfData),
      crosshair: true,
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Total No Participation',
      },
    },
    tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>'
                + '<td style="padding:0"><b>{point.y}</b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true,
    },
    plotOptions: {
      column: {
        pointPadding: 0.2,
        borderWidth: 0,
      },
    },
    series: [{
      name: 'Male',
      data: male,

    }, {
      name: 'Female',
      data: female,

    }],
  });
}
function boxingAvgAge(ageData) {
  Highcharts.chart('container4', {
    chart: {
      type: 'spline',
    },
    title: {
      text: "Average Age of Men's Boxing Pariticipents",
    },
    xAxis: {
      categories: Object.keys(ageData),
    },
    yAxis: {
      title: {
        text: 'Age',
      },
      labels: {
        formatter() {
          return parseInt(this.value, 10);
        },
      },
    },
    tooltip: {
      crosshairs: true,
      shared: true,
    },
    plotOptions: {
      spline: {
        marker: {
          radius: 4,
          lineColor: '#666666',
          lineWidth: 1,
        },
      },
    },
    series: [{
      name: 'Summer',
      marker: {
        symbol: 'square',
      },
      data: Object.values(ageData).map(key => parseInt(key, 10)),
    },
    {
      name: 'Winter',
      marker: {
        symbol: 'Daimond',
      },
      data: [],
    }],
  });
}

function indiaWinnerData(seasonData, tableTag) {
  let output = '';
  output += `<thead><tr>`;
  Object.keys(seasonData[0]).forEach((head) => { output += `<th>${head}</th>`; });
  output += '</tr></thead>';
  Object.keys(seasonData).forEach((eachRow) => {
    output += '<tr>';
    Object.values(seasonData[eachRow]).forEach((value) => {
      output += `<td>${value}</td>`;
    });
    output += '</tr>';
  });
  document.querySelector(tableTag).innerHTML = output;
}
fetch('../output/allChartsJsonData.json').then(response => response.json()).then((data) => {
  chartHostsCity(data.hostsNoPerCity);
  topMedalWinners(data.top10MedalWinners);
  mfParticipation(data.genderParticipation);
  boxingAvgAge(data.avgAgeBoxingMen);
  indiaWinnerData(data.winnersSeasonIndia.Summer, '#table-visual-summer');
  indiaWinnerData(data.winnersSeasonIndia.Winter, '#table-visual-winter');
});
