"use strict";

var functions = _interopRequireWildcard(require("./allchart"));

var _parseCsv = _interopRequireDefault(require("./parseCsv"));

var _fileio = require("./fileio");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

(0, _parseCsv.default)().then(async data => {
  let allEventsData = data.allEventsData;
  let nocData = data.nocData;

  try {
    const jsonData = {
      hostsNoPerCity: functions.noHostsYear(allEventsData),
      top10MedalWinners: functions.top10MostMedalNoc(allEventsData, 2000, nocData),
      genderParticipation: functions.mfParticipationDecade(allEventsData),
      avgAgeBoxingMen: functions.boxingMensAgeAvg(allEventsData, "Boxing Men's Heavyweight"),
      winnersSeasonIndia: functions.indiaWinners(allEventsData)
    };
    await (0, _fileio.writeFile)("../output/allChartsJsonData.json", jsonData);
  } catch (error) {
    console.log(error);
  }
});