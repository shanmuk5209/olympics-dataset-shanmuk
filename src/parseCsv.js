import csv from "csvtojson";
let convertToJson = async filePath => {
  let array = await csv().fromFile(filePath);
  return array;
};
let getJsonData = async () => {
  let allEventsData = await convertToJson("../data/athlete_events.csv");
  let nocData = await convertToJson("../data/noc_regions.csv");
  return {
    allEventsData,
    nocData
  }
};
export default getJsonData;