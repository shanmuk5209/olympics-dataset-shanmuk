
//Question1 ->Number of times olympics hosted per noc over the years - Piechart
export const noHostsYear = (allEventsData) => {
    var filteredData = allEventsData.reduce((totalEvents, eachEvent) => {
        if (totalEvents.hasOwnProperty(eachEvent['City'])) {
            if (totalEvents[eachEvent['City']].hasOwnProperty(eachEvent['Year'])) {
                if (totalEvents[eachEvent['City']][eachEvent['Year']].Season != eachEvent['Season']) {
                    totalEvents[eachEvent['City']].count++;
                }
            }
            else {
                totalEvents[eachEvent['City']].count++;
                totalEvents[eachEvent['City']][eachEvent['Year']] = {};
                totalEvents[eachEvent['City']][eachEvent['Year']].Season = eachEvent['Season'];
            }
        }
        else {
            totalEvents[eachEvent['City']] = {};
            totalEvents[eachEvent['City']].count = 1;
            totalEvents[eachEvent['City']][eachEvent['Year']] = {};
            totalEvents[eachEvent['City']][eachEvent['Year']].count = 1;
            totalEvents[eachEvent['City']][eachEvent['Year']].Season = eachEvent['Season'];
        }
        return totalEvents;
    }, {});
    return Object.keys(filteredData).reduce((allCity, eachCity) => {
        allCity[eachCity] = filteredData[eachCity].count
        return allCity
    }, {});

}

//Question2-Top 10 countries who have won most medals after 2000 - stacked column - split gold/silver/bronze
export const top10MostMedalNoc = (allEventsData, year, nocData) => {
    var medalsData = allEventsData.filter(key => {
        if (key['Year'] > year && key['Medal'] != 'NA') {
            return key;
        }
    }).reduce((totalMedals, eachMedal) => {
        if (totalMedals.hasOwnProperty(eachMedal['NOC'])) {
            totalMedals[eachMedal['NOC']][eachMedal['Medal']] += 1;
            totalMedals[eachMedal['NOC']].count += 1;
        } else {
            totalMedals[eachMedal['NOC']] = {};
            totalMedals[eachMedal['NOC']]['Gold'] = 0;
            totalMedals[eachMedal['NOC']]['Silver'] = 0;
            totalMedals[eachMedal['NOC']]['Bronze'] = 0;
            totalMedals[eachMedal['NOC']][eachMedal['Medal']] = 1;
            totalMedals[eachMedal['NOC']].count = 1;
        }

        return totalMedals;
    }, {});
    var top10Winners = Object.keys(medalsData).sort(function (min, max) {
        return medalsData[max].count - medalsData[min].count;
    }).slice(0, 10);
    return top10Winners.reduce((allNoc, eachNoc) => {
        delete medalsData[eachNoc].count;
        var countary = Object.keys(nocData).find(keyNoc => {
            if (nocData[keyNoc]['NOC'] == eachNoc) {
                return keyNoc;
            }
        }, {});
        allNoc[nocData[countary]['region']] = medalsData[eachNoc];
        return allNoc;
    }, {});
} //Question3-M/F participation by decade - column chart


export const mfParticipationDecade = (allEventsData) => {
    var mfParticipation = allEventsData.reduce((allParts, eachPart) => {
        let startYear = parseInt(parseInt(eachPart['Year']) / 10) * 10;
        let endYear = parseInt(parseInt(eachPart['Year']) / 10 + 1) * 10 - 1;
        let decade = startYear + "-" + endYear;

        if (allParts.hasOwnProperty(decade)) {
            if (allParts[decade]['Check'].hasOwnProperty(eachPart['Games'])) {
                if (!allParts[decade]['Check'][eachPart['Games']].hasOwnProperty(eachPart['ID'])) {
                    allParts[decade][eachPart['Sex']]++;
                    allParts[decade]['Check'][eachPart['Games']][eachPart['ID']] = "unique";
                }
            } else {
                allParts[decade][eachPart['Sex']]++;
                allParts[decade]['Check'][eachPart['Games']] = {};
                allParts[decade]['Check'][eachPart['Games']][eachPart['ID']] = "unique";
            }
        } else {
            allParts[decade] = {};
            allParts[decade]['M'] = 0;
            allParts[decade]['F'] = 0;
            allParts[decade]['Check'] = {};
            allParts[decade]['Check'][eachPart['Games']] = {};
            allParts[decade]['Check'][eachPart['Games']][eachPart['ID']] = "unique";
            allParts[decade][eachPart['Sex']] = 1;
        }

        return allParts;
    }, {});
    let sortedDecade = Object.keys(mfParticipation).sort((min, max) => {
        delete mfParticipation[min]['Check'];
        delete mfParticipation[max]['Check'];
        return parseInt(min) - parseInt(max);
    });
    return sortedDecade.reduce((allDecades, eachDecade) => {
        if (!allDecades.hasOwnProperty(eachDecade)) {
            allDecades[eachDecade] = {};
            allDecades[eachDecade]['Male'] = mfParticipation[eachDecade]['M'];
            allDecades[eachDecade]['Female'] = mfParticipation[eachDecade]['F'];
        }

        return allDecades; // return allDecades=eachDecade;
    }, {});
} //Question4-Per season average age of athletes who participated in Boxing Men’s Heavyweight - Line Boxing Men's Heavyweight


export const boxingMensAgeAvg = (allEventsData, eventName) => {
    var menAgeData = allEventsData.filter((eachEvent) => { if (eachEvent['Event'] == eventName && eachEvent['Age'] != "NA") { return eachEvent } }).reduce((allAges, eachAge) => {

        if (allAges.hasOwnProperty(eachAge['Year'])) {
            if (!allAges[eachAge['Year']].hasOwnProperty(eachAge['ID'])) {
                allAges[eachAge['Year']][eachAge['ID']] = parseInt(eachAge['Age']);
                allAges[eachAge['Year']].age += allAges[eachAge['Year']][eachAge['ID']]
            }
        }
        else {
            allAges[eachAge['Year']] = {};
            allAges[eachAge['Year']][eachAge['ID']] = parseInt(eachAge['Age']);
            allAges[eachAge['Year']].age = allAges[eachAge['Year']][eachAge['ID']]
        }
        return allAges;
    }, {});
    return Object.keys(menAgeData).reduce((allYears, eachYear) => {
        allYears[eachYear] = parseFloat(menAgeData[eachYear].age / (Object.keys(menAgeData[eachYear]).length - 1)).toFixed(2);
        return allYears;
    }, {});
}
//Question5-Find out all medal winners from India per season - Table
export const indiaWinners = (allEventsData) => {
    return allEventsData.filter((winner) => { if (winner['NOC'] == "IND" && winner['Medal'] != "NA") { return winner } }, {}).reduce((allWinners, eachWinner) => {
        if (allWinners.hasOwnProperty(eachWinner['Season'])) {
            allWinners[eachWinner['Season']].push(eachWinner);
        }
        else
            allWinners[eachWinner['Season']] = [];
        allWinners[eachWinner['Season']].push(eachWinner);
        return allWinners;
    }, {});
}

