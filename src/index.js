import * as functions from './allchart';
import getJsonData from './parseCsv'
import { writeFile } from "./fileio";
getJsonData().then(async data => {
    let allEventsData = data.allEventsData;
    let nocData = data.nocData;
    try {
        const jsonData = {
            hostsNoPerCity: functions.noHostsYear(allEventsData),
            top10MedalWinners: functions.top10MostMedalNoc(allEventsData, 2000, nocData),
            genderParticipation: functions.mfParticipationDecade(allEventsData),
            avgAgeBoxingMen: functions.boxingMensAgeAvg(allEventsData,"Boxing Men's Heavyweight"),
            winnersSeasonIndia: functions.indiaWinners(allEventsData),
        };
        await writeFile(
            "../output/allChartsJsonData.json",
            jsonData
        );
    }
    catch (error) {
        console.log(error);
    }
});

